﻿Ext.define('App.model.Result', {
    extend: 'Ext.data.Model',
    fields: [
    	{ name: 'type', type: 'string' },
        { name: 'name', type: 'string' },
        { name: 'value', type: 'string' }
    ]
});