﻿Ext.define('App.store.Searchs', {
    extend: 'Ext.data.Store',
    model: 'App.model.Search',
    alias: 'store.Searchs',
    proxy: {
        type: 'ajax',
        url: '/search',
        method:'GET'
    }
});
