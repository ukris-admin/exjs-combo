﻿
function capitalizeFirstLetter(string) {
    return string.charAt(0).toUpperCase() + string.slice(1);
}

var panels = [
        {
            type:'all',
            name:'<h3>All</h3>'
        },
        {
            type:'reports',
            name:'<h3>Reports</h3>'
        },
        {
            type:'dashboards',
            name:'<h3>Dashboards</h3>'
        }
    ],
    resultPanels = [],
    panelTypes = [null, "reports", "dashboards"];

panels.forEach(function(panel) {
    var uiPanel = Ext.create('Ext.panel.Panel', {
        title  : panel.name,
        layout : {
            type  : 'vbox'
        },
        items : [{
            xtype : 'panel',
            width : '100%',
            padding: 20,
            flex  : 1,
            cls: 'two-column'
        }]
    });

    resultPanels.push(uiPanel);
});

var tabPanel = Ext.create('Ext.tab.Panel', {
    xtype: 'basic-tabs',
    width : '100%',
    cls: 'custom-panel',
    scrollable: true,
    items: [
		resultPanels[0],
       resultPanels[1],
       resultPanels[2]
    ]
});

var userEnteredKey = "";

Ext.define('App.view.Viewport', {
    extend: 'Ext.form.Panel',
    xtype: 'custom-template-combo',
    layout: {
        type: 'vbox',
        align: 'stretch',
    },
    viewModel: {},
    items: [{
		layout: 'column',
        cls: 'custom-header',
        items: [{
			    columnWidth: .5,
			    layout: 'form',
                items: [{}]
		    }, {
                columnWidth: .5,
			    layout: 'form',
                items: [{
                    displayField: 'title',
                    hideTrigger: true,
                    xtype: 'combo',

                    reference: 'connector',
                    publishes: 'value',

                    fieldStyle : 'font-family: FontAwesome;',
                    queryMode: 'local',
        			emptyText: '\uF002 Search...',
                    store: {
                        type: 'Searchs',
                        autoLoad: true
                    },
                    cls: 'normal-search',
                    tpl: Ext.create('Ext.XTemplate',
                        '{[this.currentKey = null]}',
                        '<tpl for=".">',
                            '<tpl if="this.shouldShowHeader(type)">',
                                '<h3 class="group-header">{[this.showHeader(values.type)]}</h3>',
                            '</tpl>',
                            '<div class="x-boundlist-item">{title}</div>',
                        '</tpl>',
                        {
                            shouldShowHeader: function(type){
                                return this.currentKey != type;
                            },
                            showHeader: function(type){
                                this.currentKey = type;
                                return type;
                            }
                        }
                    ),

                    listeners: {

                        select: function(combobox) {
                            var resultStore = Ext.getStore("Results");
                            userEnteredKey = combobox.getValue();
                            resultStore.load({
                                params: {
                                    keys: combobox.getValue()
                                },
                                callback: function(records, operation, success) {
                                    var type,
                                        tabContent = {},
                                        allContents = [],
                                        type_no,
                                        no_results = true;

                                    resultStore.each(function(item, index) {
                                        if (!tabContent[item.data.type]) {
                                            tabContent[item.data.type] = [];
                                            var itemHead =
                                                '<div class="result-heading">' +
                                                    '<h2>' + capitalizeFirstLetter(item.data.type) + '</h2>' +
                                                '</div>';
                                            tabContent[item.data.type].push(itemHead);
                                        }

                                        var itemDetail =
                                            '<div class="detail-result">' +
                                                '<h3>' + item.data.name + '</h3>' +
                                                '<div>' + item.data.value + '</div>' +
                                            '</div>';
                                        tabContent[item.data.type].push(itemDetail);
                                    });

                                    // group tab content in all content;
                                    for (var i in tabContent) {
                                        console.log('tab content', i);
                                        allContents.push('<div class="group-content">');
                                        allContents.push(tabContent[i].join(''));
                                        allContents.push('</div>')
                                    }


                                    for (type_no = 0; type_no < resultPanels.length; type_no++) {
                                        type = panelTypes[type_no];

                                        if (type && tabContent[type]) {
                                            tabPanel.setActiveTab(type_no);
                                            no_results = false;
                                            resultPanels[type_no].items.get(0).update(tabContent[type].join(''));
                                        } else {
                                            tabPanel.setActiveTab(0);
                                            resultPanels[type_no].items.get(0).update('');
                                        }
                                    }

                                    if (no_results) {

                                        if (!tabContent['suggestions']) {
                                            allContents = [
                                                '<div class="result-heading">'
                                                    + 'Your search - '
                                                        +' <span class="search-key">'
                                                            + userEnteredKey
                                                        + '</span> '
                                                    + ' did not match any documents'
                                                + '</div>'];

                                        } else {
                                            allContents.unshift(
                                                '<div class="result-heading">'
                                                    + 'Your search - </h3>'
                                                        + '<span class="search-key">'
                                                            + userEnteredKey
                                                        + '</span>'
                                                    + ' did not match any documents'
                                                + '</div>'
                                            );
                                        }
                                    }

                                    resultPanels[0].items.get(0).update(allContents.join(''));

                                }
                            });
                        },

                        change: function(combobox , newValue , oldValue , eOpts) {
                            var searchStore = Ext.getStore('Searchs');
                            userEnteredKey = combobox.getValue();
                            searchStore.load({
                                params: {
                                    keys: userEnteredKey
                                }
                            })
                        },

                        focus: function(combobox) {
                            console.log('combobox', combobox.el);

                            combobox.el.animate({
                                to: {
                                    width: 500
                                }
                            });
                        },

                        blur: function(combobox) {
                            combobox.el.animate({
                                to: {
                                    width: 150
                                }
                            });
                        }
                    }

                }]
        }]
    }, tabPanel]
});
