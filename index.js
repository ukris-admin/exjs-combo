'use strict';

var express = require('express'),
	app = express(),
	fs = require('fs'),
	path = require('path'),
	http,
	config = {
		secure:false,
		port:8002
	};

app.use('/',express.static(path.resolve(__dirname,'public')));

function response(res,filePath,filter) {
	var file = fs.readFileSync('data/'+filePath+'.json','utf8'),
		result = JSON.parse(file);
	if (filter) {
		filter = filter.toUpperCase();
		var o,v;
		result = result.filter(function(res) {
			for (o in res) {
				v = res[o].toUpperCase();
				if (v.indexOf(filter) !== -1) {
					console.log('Match ' + v);
					return true;
				}
			}

			return false;	
		})
	}
	res.status(200).send(result);	
}

app.get('/',function(req,res) {
	res.sendFile(path.join(__dirname+'/index.html'));
});	

app.get('/search',function(req,res) {
	return(response(res,'ndata',req.query.keys));
});

app.get('/results',function(req,res) {
	return(response(res,'results',req.query.keys));
});

http = require('http').createServer(app);

http.listen(config.port,function() {
	console.log('Advanced Search Server running in Port '+config.port);
})
